
from pyspark.ml.feature import OneHotEncoderEstimator
from .abstract_component import AbstractSparkPipelineEstimator


class CustomerOneHotEncoderEstimator(OneHotEncoderEstimator, AbstractSparkPipelineEstimator):

    def __init__(self, name, run_id, inputCols=None, outputCols=None, handleInvalid='error', dropLast=True):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} component_name={self.name} with run_id {run_id}', flush=True)
        OneHotEncoderEstimator.__init__(self, inputCols=inputCols, outputCols=outputCols, handleInvalid=handleInvalid, dropLast=dropLast)
        AbstractSparkPipelineEstimator.__init__(self, run_id)
    
    def _fit(self, dataset):
        self.state = dataset
        args = [self.run_id]
        self.start(*args)
        print(f"In {self.__class__.__name__} _fit, now calling super's _fit", flush=True)
        model = super()._fit(dataset)
        self.completed()
        print(f'Completed component: {self.name}', flush=True)
        return model